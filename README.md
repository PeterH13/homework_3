Title: Colorful Ball Mirror

Screenshot:

<img width="1016" alt="Screen Shot 2020-01-28 at 12 50 19 PM" src="https://user-images.githubusercontent.com/624713/73300131-b771a180-41cd-11ea-882d-36763098a6b4.png">

Description: I used an example that uses pixels to create ofCircles and added a GUI that controls 
the color of the circles and the number of circles drawn in the Y direction.