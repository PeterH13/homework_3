#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    cam.setup(1280, 720);
    frame.allocate(800, 600, OF_IMAGE_COLOR);
    gui.setup();
    gui.add(slider1.setup("Number of Circles", 10, 3, 30));
    gui.add(slider2.setup("Color", 150, 0, 255));
}

//--------------------------------------------------------------
void ofApp::update(){
    if(cam.isInitialized()){
        cam.update();
        if(cam.isFrameNew()){
            frame.setFromPixels(cam.getPixels());
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    for(int x = 50; x < 1280; x+= 10){
        for(int y = 50; y<720; y+=slider1){
            float bright = frame.getColor(x,y).getBrightness();
            ofSetColor(ofColor::fromHsb(slider2, 255, 255));
            ofDrawEllipse(x, y, bright/5, bright/5);
            gui.draw();
        }
    }
}

