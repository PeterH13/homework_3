#pragma once

#include "ofMain.h"

#include "ofxGui.h"

class ofApp : public ofBaseApp{
    
public:
    void setup();
    void update();
    void draw();
    
    ofVideoGrabber cam;
    ofImage frame;
    ofxPanel gui;
    ofxIntSlider slider1;
    ofxIntSlider slider2;
};
